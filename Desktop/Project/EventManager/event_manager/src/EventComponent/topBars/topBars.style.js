import styled from "styled-components";

const DIV = styled.div`
  background-color: #596b8c;

  .navs {
    width: 100vw;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #596b8c;
  }

  .nav-links {
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    padding: 10px 30px;
  }

  .links {
    background-color: transparent;
    text-decoration: none;
    color: white;
    padding: 12px 20px;
  }

  .actives {
    color: rgb(212, 171, 171);
    border-bottom: 1px solid rgb(212, 171, 171);
  }

  .handbugbody {
    color: white;
  }

  .handbug {
    width: 30px;
    height: 30px;
    display: none;
  }

  @media (max-width: 800px) {
    .nav-links {
      padding: 10px 0px;
    }

    .links {
      padding: 12px 8px;
    }
  }

  @media (max-width: 414px) {
    .handbugbody {
      padding: 5px 20px;
    }

    .handbug {
      display: block;
      padding: 10px;
    }

    .navs {
      position: fixed;
      display: ${({ view }) => (view === false ? "none" : "flex")};
      flex-direction: column;
      align-items: flex-start;
    }

    .nav-links {
      flex-direction: column;
      align-items: flex-start;
      padding: 10px 10px;
    }
  }
`;

export default DIV;

import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./topBars.style.js";
import { FaBars } from "react-icons/fa";
import DIV from "./topBars.style";

const TopBars = () => {
  const [show, setShow] = useState(false);

  const handleClickTrue = () => {
    setShow(!show);
  };

  const handleClickFalse = () => {
    setShow(false);
  };

  return (
    <DIV view={show}>
      <div className={"handbugbody"}>
        <FaBars className={"handbug"} onClick={handleClickTrue} />
      </div>
      <div className={"navs"}>
        <div className={"nav-links"}>
          <NavLink
            to="/"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            Home
          </NavLink>
          <NavLink
            to="/about"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            About
          </NavLink>
          <NavLink
            to="/service"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            Service
          </NavLink>
          <NavLink
            to="/contact"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            Contact
          </NavLink>
        </div>
        <div className={"nav-links"}>
          <NavLink
            to="/signin"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            SignIn
          </NavLink>
          <NavLink
            to="/signup"
            className={"links"}
            activeClassName="actives"
            onClick={handleClickFalse}
          >
            SignUp
          </NavLink>
        </div>
      </div>
    </DIV>
  );
};

export default TopBars;

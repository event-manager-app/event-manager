import React from "react";
// import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import auth from "../../images/auth.jpg";
import "./logs.css";

const Log = ({
  signArray,
  handleChange,
  handleSubmit,
  Title,
  discription,
  signD,
  sign,
  signLink,
  message,
}) => {
  return (
    <div className={"log"}>
      <div>
        <img src={auth} alt="login" className={"logImage"} />
      </div>
      <div className={"logForm"}>
        <div>
          <div className={"logTitle"}>
            <h2>{Title}</h2>
          </div>
          <div className={"logDescs"}>
            <p>{discription}</p>
          </div>

          {message && (
            <div className={"logDescs message"}>
              <p>{message}</p>
            </div>
          )}
        </div>
        <form className={"formfield"} onSubmit={handleSubmit}>
          <div>
            {signArray.map((item, key) => (
              <div className={"logDiv"} key={key}>
                <input
                  type={item.type}
                  name={item.name}
                  value={item.value}
                  placeholder={item.placeholder}
                  onChange={handleChange}
                  className={item.className}
                />
                <div>
                  <div className={"logDivF"}>
                    <p>
                      {item.reminder}
                      <span className={"logSpan"}>
                        <Link className={"logLink"} to="">
                          {item.reminder_link}
                        </Link>
                      </span>
                    </p>
                    <Link className={"logLink"} to="">
                      {item.reminder_password}
                    </Link>
                  </div>
                </div>
              </div>
            ))}
            <div className={"logfooter"}>
              <p>
                {signD}
                <span>
                  <Link className={"logLink footerlink"} to={`/${signLink}`}>
                    {sign}
                  </Link>
                </span>
              </p>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Log;

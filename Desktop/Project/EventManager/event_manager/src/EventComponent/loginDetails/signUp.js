import { useState, useEffect } from "react";
import Log from "./logs";
import axios from "axios";
import "./logs.css";
import BaseUrl from "../../config";

const SignUp = () => {
  const [signUp, setSignUp] = useState({});
  const [message, setMessage] = useState("");
  const [userData, setUserData] = useState([]);

  useEffect(() => {
    // const getD = async () => {
    //   const SavedData = await fetchData();
    //   setUserData(SavedData);
    // };
    // getD();
  }, [message]);

  let signUps = [
    {
      type: "text",
      name: "name",
      placeholder: "Full Name",
      className: "logInput",
    },
    {
      type: "email",
      name: "email",
      placeholder: "example@mail.com",
      className: "logInput",
    },
    {
      type: "password",
      name: "password",
      placeholder: "Password...",
      className: "logInput",
    },
    {
      type: "password",
      name: "confirm_password",
      placeholder: "Comfirm Password",
      className: "logInput",
    },
    {
      type: "checkbox",
      name: "check",
      className: "checker",
      reminder: "I accept the",
      reminder_link: "terms & conditions",
    },
    {
      type: "submit",
      value: "SignUp",
      className: "logsubmit",
    },
  ];

  // const addSignUp = async (logData) => {
  //   const res = await fetch("http://localhost:5300/logs", {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify(logData),
  //   });
  // };

  const addSignUp = (logData) => {
    axios
      .post(`${BaseUrl}/registerUser`, logData)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        if (err.message === "Request failed with status code 400") {
          setMessage("This user exit");
        }
        // console.log(err);
      });
  };

  // const fetchData = async () => {
  //   const res = await fetch("http://localhost:5300/logs");
  //   const data = res.json();
  //   return data;
  // };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignUp({ ...signUp, [name]: value });

    // const getData = async () => {
    //   const SavedData = await fetchData();
    //   setUserData(SavedData);
    // };

    // getData();
    setMessage("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const getDataS = () => {
      let mst;

      const dataSet = userData.filter((item) => {
        return item.email === signUp.email;
      });

      // console.log(dataSet[0].email);

      // userData.map((item) => {
      //   if (item.email === signUp.email) {
      //     mst = true;
      //   } else {
      //     mst = false;
      //   }
      // });

      if (!signUp.email) {
        return setMessage("email is empty");
      }

      if (dataSet.length !== 0) {
        return setMessage("This email exit");
      }

      if (mst) {
        return setMessage("This email exit");
      }

      if (signUp.password.length <= 6) {
        return setMessage("password length must be greater than 6");
      }

      if (signUp.password !== signUp.confirm_password) {
        return setMessage("password and confirm password is not equal");
      }

      if (dataSet.length === 0) {
        addSignUp(signUp);
      }

      // if (!mst) {
      //   addSignUp(signUp);
      // }

      // const getD = async () => {
      //   const SavedData = await fetchData();
      //   setUserData(SavedData);
      // };
      // getD();

      setMessage("");
    };

    getDataS();
  };

  return (
    <div>
      <Log
        Title="Welcome To GodFirst Event Tracker"
        discription="Create And Account Today"
        signArray={signUps}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        signD="Already have account?"
        sign="SignIn"
        signLink="signin"
        message={message}
      />
    </div>
  );
};

export default SignUp;

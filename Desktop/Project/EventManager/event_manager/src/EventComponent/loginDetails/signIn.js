import { useState } from "react";
import Log from "./logs";
import { useNavigate } from "react-router-dom";
import "./logs.css";
import axios from "axios";
import BaseUrl from "../../config";

const SignIn = () => {
  const navigate = useNavigate();
  let signIns = [
    {
      type: "email",
      name: "email",
      placeholder: "example@mail.com",
      className: "logInput",
    },
    {
      type: "password",
      name: "password",
      placeholder: "Password...",
      className: "logInput",
    },
    {
      type: "checkbox",
      name: "check",
      className: "checker",
      reminder: "Remember Me",
      reminder_password: "Forgot Password?",
    },
    {
      type: "submit",
      value: "SignIn",
      className: "logsubmit",
    },
  ];
  const [signIn, setSignIn] = useState({});
  const [userData, setUserData] = useState([]);
  const [message, setMessage] = useState("");
  const useremail = signIn.email;

  const fetchDataS = (logData) => {
    axios
      .post(`${BaseUrl}/login`, logData)
      .then((res) => {
        sessionStorage.setItem("user-details", JSON.stringify(res.data.user));
        setUserData(res.data);
        sessionStorage.setItem("userAccess", res.data.access);
        if (res.data.msg === "Success") {
          navigate(`/user`);
        }
      })
      .catch((err) => {
        if (err.message === "net::") {
          setMessage("fail to connect to the database");
        } else if (err.message === "Request failed with status code 401") {
          setMessage("email or password error");
        }
      });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignIn({ ...signIn, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetchDataS(signIn);
  };

  return (
    <div>
      <Log
        Title="Welcome To GodFirst Event Tracker"
        discription="Welcome Back, Please login
        to your account."
        signArray={signIns}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        signD={`Don't Have and Account`}
        sign="SignUp"
        signLink="signup"
        message={message}
      />
    </div>
  );
};

export default SignIn;

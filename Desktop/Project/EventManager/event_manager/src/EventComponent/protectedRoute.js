import { Outlet, Navigate, useNavigate, useLocation } from "react-router-dom";

const ProtectedRoute = ({ accessKey, children }) => {
  // return accessKey ? (
  //   <Outlet />
  // ) : (
  //   <Navigate to="/" replace />
  // );
  if (!accessKey) {
    return <Navigate to="/" replace />;
  }
  return children ? children : <Outlet />;
};

export default ProtectedRoute;

import axios from "axios";
import React from "react";
import BaseUrl from "../config";

const Error = () => {
  const access = sessionStorage.getItem("userAccess");
  console.log(access);
  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/thismonth`, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        console.log(res);
        // setEventL(res.data.data);
      })
      .catch((err) => {
        console.log(err, "from here");
      });
  };

  return (
    <div>
      <button onClick={fetchEvent}>check</button>
    </div>
  );
};

export default Error;

import styled from "styled-components";

export const DIV = styled.div`
  font-family: "NeueDisplay" sans-serif;
  position: sticky;
  top: 0px;
  left: 0px;
  bottom: 0px;
  width: 250px;
  height: 100vh;
  background-color: white;

  @font-face {
    font-family: "NeueDisplay";
    src: url(../../../neue_haas_sv/NHaasGroteskTXPro-55Rg.ttf)
      format("truetype");
  }

  .sideBar-header {
    border: 1px solid rgb(212, 171, 171);
    padding: 8px 12px;
    color: #596b8c;
    margin-bottom: 20px;
    align-items: center;
  }

  .nav-link {
    /* background-color: tomato; */
    padding: 10px 0px;
    display: flex;
    flex-direction: column;
  }

  .link {
    padding: 14px 20px;
    background-color: #f8fafb;
    /* border-radius: 12px; */

    outline: none;
    margin: 16px 0px;
    text-decoration: none;
    color: #596b8c;
  }

  .active {
    border-left: 6px solid #b1a4e0;
  }
  .sideBarCloseIcon {
    display: none;
  }

  @media (max-width: 720px) {
    height: 100%;
    display: ${({ view }) => (view == true ? "block" : "none")};
    position: fixed;
    z-index: 3;

    .sideBar-header {
      display: flex;
      justify-content: space-between;
      text-align: center;
    }

    .sideBarCloseIcon {
      width: 24px;
      height: 24px;
      display: block;
    }
  }

  @media (max-width: 416px) {
    display: ${({ view }) => (view == true ? "block" : "none")};
    position: fixed;
    z-index: 3;

    .sideBar-header {
      display: flex;
      justify-content: space-between;
      text-align: center;
    }

    .sideBarCloseIcon {
      width: 24px;
      height: 24px;
      display: block;
    }
  }
`;

export const SubDiv = styled(DIV)`
  width: 160px;
`;

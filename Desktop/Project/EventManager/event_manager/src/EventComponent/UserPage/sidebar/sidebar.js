import React, { useState, useEffect } from "react";
import {
  FaServer,
  FaPlusCircle,
  FaThList,
  FaRegWindowMaximize,
  FaRecycle,
  FaKaaba,
  FaTimes,
} from "react-icons/fa";
import { NavLink, useParams } from "react-router-dom";
import { DIV, SubDiv } from "./sidebar.style.js";

const SideBar = ({ reduce, showSideBar, onclick }) => {
  let { useremail, id } = useParams();
  const [respond, setRespond] = useState();

  return (
    <DIV view={showSideBar}>
      <div>
        {reduce ? (
          <SubDiv view={showSideBar}>
            <div className={"sideBar-header"}>
              <h2>Event Tracker</h2>
              <div onClick={onclick}>
                <FaTimes className={"sideBarCloseIcon"} />
              </div>
            </div>
            <div className={"nav-link"}>
              <NavLink
                to={`/user`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaServer />
              </NavLink>
              <NavLink
                to={`/create`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaPlusCircle />
              </NavLink>
              <NavLink
                to={`/allevent`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaThList />
              </NavLink>
              <NavLink
                to={`/present`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaRecycle />
              </NavLink>
              <NavLink
                to={`/future`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaRegWindowMaximize />
              </NavLink>
              <NavLink
                to={`/passed`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                <FaKaaba />
              </NavLink>
            </div>
          </SubDiv>
        ) : (
          <div>
            <div className={"sideBar-header"}>
              <h2>Event Tracker</h2>
              <div onClick={onclick}>
                <FaTimes className={"sideBarCloseIcon"} />
              </div>
            </div>
            <div className={"nav-link"}>
              <NavLink
                to={`/user`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                Dashboard
              </NavLink>
              <NavLink
                to={`/create`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                Create Event
              </NavLink>
              <NavLink
                to={`/allevent`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                All Event
              </NavLink>
              <NavLink
                to={`/present`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                Today's Event
              </NavLink>
              <NavLink
                to={`/future`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                Future Event
              </NavLink>
              <NavLink
                to={`/passed`}
                className={"link"}
                activeclassname="active"
                onClick={onclick}
              >
                Previous Event
              </NavLink>
            </div>
          </div>
        )}
      </div>
      {/* )} */}
    </DIV>
  );
};

export default SideBar;

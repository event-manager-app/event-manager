import React, { useState, useEffect } from "react";
import FutureE from "./futureE";
import "./future.css";
import axios from "axios";
import BaseUrl from "../../../config";

const Future = () => {
  const [searchDetails, setSearchDetails] = useState([]);
  const [searchEvent, setSearchEvent] = useState([]);
  const [query, setQuery] = useState("");

  const access = sessionStorage.getItem("userAccess");
  useEffect(() => {
    fetchEvent();
  }, []);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/future`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setSearchEvent(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    let resentSearch = searchEvent.filter((item) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );

    setSearchDetails(resentSearch);
  };

  return (
    <div className={"futures"}>
      <div className="allListInput">
        <input
          className={"futureInput"}
          type="text"
          value={query}
          placeholder={"Search for Event"}
          onChange={handleChange}
        />
      </div>
      <div className={"allListHeader"}>
        <h2>FUTURE EVENT</h2>
      </div>
      <FutureE futureCount={searchDetails} futureQuery={query} />
    </div>
  );
};

export default Future;

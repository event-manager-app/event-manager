import { useState, useEffect } from "react";
import EventDiv from "../eventDiv/eventDiv";
import "./future.css";
import EventList from "../Event/EList/EventList";

const FutureE = ({ direction, len, futureCount, futureQuery }) => {
  return (
    <div>
      <EventList
        len={len}
        listCount={futureCount}
        listQuery={futureQuery}
        listSequence={"future"}
      />
    </div>
  );
};

export default FutureE;

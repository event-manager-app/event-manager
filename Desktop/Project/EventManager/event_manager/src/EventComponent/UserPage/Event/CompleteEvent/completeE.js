import React from "react";
import { FaPlusCircle } from "react-icons/fa";
import Complete from "../CompleteEvent/Complete";
import "../EList/EventList.css";
import { Link, useParams } from "react-router-dom";

const CompleteE = () => {
  const { useremail } = useParams();
  const direction = "row";
  return (
    <div>
      <div className={"HeadEvent"}>
        <FaPlusCircle />
        <h3>Previous Events</h3>
      </div>
      <Complete direction={direction} len={2} passQuery={""} />
      <Link className={"seeMore"} to={`/user/${useremail}/passed`}>
        View More...
      </Link>
    </div>
  );
};

export default CompleteE;

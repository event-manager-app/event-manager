import React from "react";
import { FaPlusCircle } from "react-icons/fa";
import PList from "./PList";
import "../EList/EventList.css";
import { Link, useParams } from "react-router-dom";

const Progress = () => {
  const { useremail } = useParams();
  const direction = "row";
  return (
    <div>
      <div className={"HeadEvent"}>
        <FaPlusCircle />
        <h3>Today Events</h3>
      </div>
      <PList direction={direction} len={2} presentQuery={""} />
      <Link className={"seeMore"} to={`/user/${useremail}/present`}>
        View More...
      </Link>
    </div>
  );
};

export default Progress;

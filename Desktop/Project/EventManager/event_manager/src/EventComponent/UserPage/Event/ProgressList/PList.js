import axios from "axios";
import { useState, useEffect } from "react";
import BaseUrl from "../../../../config";
import EventDiv from "../../eventDiv/eventDiv";
import EventList from "../EList/EventList";

const PList = ({ direction, len, presentCount, presentQuery }) => {
  const [progressList, setProgList] = useState([]);
  const access = sessionStorage.getItem("userAccess");

  var today = new Date();

  var date = `${today.getFullYear()}-0${
    today.getMonth() + 1
  }-0${today.getDate()}`;

  const present = progressList.filter(
    (item) => Date.parse(item.stime) === Date.parse(date)
  );

  return (
    <div>
      <EventList
        listCount={presentCount}
        listQuery={presentQuery}
        listSequence={"today"}
        len={len}
      />
    </div>
  );
};

export default PList;

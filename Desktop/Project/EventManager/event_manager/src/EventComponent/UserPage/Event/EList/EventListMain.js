import React from "react";
import { FaPlusCircle } from "react-icons/fa";
import EventList from "./EventList";
import "./EventList.css";
import { Link, useParams } from "react-router-dom";

const EventListMain = () => {
  const { useremail } = useParams();

  const direction = "row";

  return (
    <div>
      <div className={"HeadEvent"}>
        <FaPlusCircle />
        <h3>All Event</h3>
      </div>
      <EventList
        direction={direction}
        len={2}
        listQuery={""}
        listSequence={"allevents"}
      />
      <Link className={"seeMore"} to={`/user/allevent`}>
        View More...
      </Link>
    </div>
  );
};

export default EventListMain;

import { FaTimes } from "react-icons/fa";
import "./EventForm.css";
import axios from "axios";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import BaseUrl from "../../../../config";

const EventForm = ({
  onAddData,
  show,
  onClose,
  formEdit,
  editId,
  fetchEvt,
}) => {
  const [formD, setFormD] = useState();
  const [editForm, setEditForm] = useState();
  const [formMessage, setFormMessage] = useState("");

  if (!show) {
    return null;
  }

  const id = uuidv4();

  const access = sessionStorage.getItem("userAccess");

  const addEvents = (events) => {
    axios
      .post(`${BaseUrl}/events/new`, events, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const FormEditor = () => {
    axios
      .put(`${BaseUrl}/events/${editId}`, formD, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  let HandleSubmit = async (e) => {
    e.preventDefault();
    console.log(id);

    if (formD.name === undefined) {
      setFormMessage("Your Title is Missing");
    }

    if (formD.name !== undefined || formD.description !== undefined) {
      {
        formEdit ? FormEditor() : await addEvents(formD);
      }
      setFormD({});
      onClose();
      setFormMessage("");
    }
    fetchEvt();
  };

  let HandleChange = (e) => {
    const { name, value } = e.target;

    setFormD({ ...formD, [name]: value });
  };

  return (
    <div className={"modal modal_animation"}>
      <form className={"form"} onSubmit={HandleSubmit} className={"form"}>
        <div className={"formHeader"}>
          <h3>New Task</h3>
          {formMessage.length !== 0 && (
            <p className={"message1"}>{formMessage}</p>
          )}
          <FaTimes
            className={"close"}
            onClick={() => {
              onClose();
              setFormMessage("");
            }}
          />
        </div>
        <div className={"detail"}>
          <div className={"details"}>
            <label className={"label1"}>Title:</label>
            {
              <input
                className={"input1"}
                type="text"
                name="name"
                defaultValue={formEdit && formEdit.name}
                placeholder="Enter Title"
                onChange={HandleChange}
              />
            }
          </div>
          <div className={"details dSize"}>
            <label className={"label"}>Starting Time:</label>
            <input
              className={"time"}
              type="time"
              name="etime"
              defaultValue={formEdit && formEdit.etime}
              placeholder="Enter Title"
              onChange={HandleChange}
            />
          </div>
          <div className={"details dSize"}>
            <label className={"label1"}>Date:</label>
            <input
              className={"time dat"}
              type="date"
              name="stime"
              defaultValue={formEdit && formEdit.stime}
              placeholder="Enter Title"
              onChange={HandleChange}
            />
          </div>
          <div className={"details"}>
            <label className={"label1"}>Description:</label>
            <textarea
              placeholder="Enter Title"
              cols="10"
              rows="10"
              name="description"
              defaultValue={formEdit && formEdit.description}
              onChange={HandleChange}
            ></textarea>
          </div>
        </div>
        <div className={"details"}>
          <input className={"submit"} type="submit" value="SUBMIT" />
        </div>
      </form>
    </div>
  );
};

export default EventForm;

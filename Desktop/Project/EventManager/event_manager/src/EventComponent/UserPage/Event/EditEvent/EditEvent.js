import axios from "axios";
import { useState, useEffect } from "react";
import BaseUrl from "../../../../config";
import EventForm from "../EventForm/EventForm";
import "./EditEvent.css";

const EditEvent = ({
  eEdit,
  onClosed,
  passId,
  fetchEvt,
  onEdits,
  showForm,
  publics,
  date,
}) => {
  const [show, setShow] = useState(false);
  const access = sessionStorage.getItem("userAccess");

  if (!eEdit) {
    return null;
  }

  let today = new Date();

  let dates = `${today.getFullYear()}-0${
    today.getMonth() + 1
  }-${today.getDate()}`;

  const handleChange = () => {
    let makePublic = { public: !publics };
    axios
      .put(`${BaseUrl}/events/${passId}`, makePublic, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        fetchEvt();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className={"EditBody"}>
      <div className={"EditDetails"}>
        <input
          type="checkbox"
          className={"public"}
          checked={publics === true ? true : false}
          onChange={handleChange}
        />
        <button>public</button>
      </div>

      <div
        className={"EditDetails"}
        onClick={() => {
          {
            dates < date && onEdits(passId);
          }
          {
            dates < date && setShow(!show);
          }
        }}
        style={{ backgroundColor: `${dates >= date && "#596b8c"}` }}
      >
        <div className={"detailBox edit"}></div>
        <button>Edit</button>
      </div>

      <EventForm
        onClose={() => setShow(false)}
        show={show}
        formEdit={showForm}
        editId={passId}
        fetchEvt={fetchEvt}
      />
      <div
        className={"EditDetails"}
        style={{ backgroundColor: `${dates >= date && "#596b8c"}` }}
        onClick={() => {
          dates < date && onClosed();
        }}
      >
        <div className={"detailBox delete"}></div>
        <button>Delete</button>
      </div>
    </div>
  );
};

export default EditEvent;

import React, { useState } from "react";
import CompleteE from "../CompleteEvent/completeE";
import EventListMain from "../EList/EventListMain";
import EventForm from "../EventForm/EventForm";
import Progress from "../ProgressList/Progress";
import "./EventBoard.css";

const EventBoard = () => {
  const [show, setShow] = useState(false);
  return (
    <div className={"Board"}>
      <div className={"Board_Header"}>
        <h2>EVENT BOARD</h2>
        <button onClick={() => setShow(true)}>ADD EVENT</button>
        <EventForm onClose={() => setShow(false)} show={show} />
      </div>
      <div className={"Bord-list"}>
        <div className={"createDiv"}>
          <EventListMain />
        </div>
        <div className={"createDiv"}>
          <Progress />
        </div>
        <div className={"createDiv"}>
          <CompleteE />
        </div>
      </div>
    </div>
  );
};

export default EventBoard;

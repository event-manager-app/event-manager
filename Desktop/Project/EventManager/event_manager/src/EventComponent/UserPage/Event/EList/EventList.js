import axios from "axios";
import { useEffect, useState } from "react";
import BaseUrl from "../../../../config";
import EventDiv from "../../eventDiv/eventDiv";

const EventList = ({ direction, len, listCount, listQuery, listSequence }) => {
  const [eventL, setEventL] = useState([]);
  const access = sessionStorage.getItem("userAccess");
  const [showForm, setShowForm] = useState({});

  useEffect(() => {
    fetchEvent();
  }, [listSequence]);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/${listSequence}`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setEventL(res.data.data);
        axios
          .get(`${BaseUrl}/events/allevents`, {
            headers: {
              "Content-Type": "application/json",
              authorization: `Bearer ${access}`,
            },
          })
          .then((res) => {
            const local = res.data.data.filter((item) => item.public === true);
            localStorage.setItem("event", JSON.stringify(local));
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onEdit = (passId) => {
    axios
      .get(`${BaseUrl}/events/allevents`, {
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        const showFormL = res.data.data.find((item) => item._id === passId);
        setShowForm(showFormL);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const DeleteEvent = (id) => {
    axios.delete(`${BaseUrl}/events/${id}`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        authorization: `Bearer ${access}`,
      },
    });
    setEventL(eventL.filter((task) => task._id !== id));
  };

  let today = new Date();

  let date = `${today.getFullYear()}-0${
    today.getMonth() + 1
  }-${today.getDate()}`;

  return (
    <div>
      {listQuery !== "" && listQuery !== null ? (
        <div
          className={"eventListDiv"}
          style={{
            flexDirection: `${direction}`,
          }}
        >
          {listCount.map(
            (item, key) =>
              key <= (len || eventL.length) && (
                <div key={key}>
                  <EventDiv
                    title={item.name}
                    id={item._id}
                    starttime={item.etime}
                    date={item.stime}
                    description={item.description}
                    showForm={showForm}
                    onEdit={onEdit}
                    publics={item.public}
                    DeleteEvent={date < item.stime && DeleteEvent}
                    fetchEvt={fetchEvent}
                  />
                </div>
              )
          )}
        </div>
      ) : (
        <div
          className={"eventListDiv"}
          style={{
            flexDirection: `${direction}`,
          }}
        >
          {eventL.map(
            (item, key) =>
              key <= (len || eventL.length) && (
                <div key={key}>
                  <EventDiv
                    title={item.name}
                    id={item._id}
                    starttime={item.etime}
                    date={item.stime}
                    description={item.description}
                    onEdit={onEdit}
                    showForm={showForm}
                    publics={item.public}
                    DeleteEvent={DeleteEvent}
                    fetchEvt={fetchEvent}
                  />
                </div>
              )
          )}
        </div>
      )}
    </div>
  );
};

export default EventList;

import { useState, useEffect } from "react";
import EventDiv from "../../eventDiv/eventDiv";
import EventList from "../EList/EventList";

const Complete = ({ direction, len, completeCount, passCount, passQuery }) => {
  const [CompleteEvent, setCompleteEvent] = useState([]);

  return (
    <div>
      <EventList
        len={len}
        listCount={passCount}
        listQuery={passQuery}
        listSequence={"previous"}
      />
    </div>
  );
};

export default Complete;

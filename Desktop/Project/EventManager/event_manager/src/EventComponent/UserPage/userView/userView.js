import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import BaseUrl from "../../../config";
import "./userView.css";

const UserView = () => {
  const { userId } = useParams();
  const [userDetails, setUserDetails] = useState([]);
  const access = sessionStorage.getItem("userAccess");

  useEffect(() => {
    fetchEvent();
  }, [access]);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/allevents`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setUserDetails(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  let resentUser = userDetails.find((item) => item._id == userId);

  return (
    <div>
      <div className="userCard">
        <div className={"dates"}>
          {resentUser && <h3>{resentUser.etime}</h3>}
          {resentUser && <h3>{resentUser.stime}</h3>}
        </div>
        <div className={"titles"}>
          {resentUser && <h2>{resentUser.name}</h2>}
        </div>
        <div className={"descriptions"}>
          {resentUser && <h3>{resentUser.description}</h3>}
        </div>
      </div>
    </div>
  );
};

export default UserView;

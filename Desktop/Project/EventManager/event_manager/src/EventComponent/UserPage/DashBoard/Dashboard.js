import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import {
  FaListAlt,
  FaCheckCircle,
  FaTimesCircle,
  FaClock,
  FaBars,
} from "react-icons/fa";
import FutureE from "../Future/futureE";
import Complete from "../Event/CompleteEvent/Complete";
import PList from "../Event/ProgressList/PList";
import "./dashboard.css";
import axios from "axios";
import BaseUrl from "../../../config";

const Dashboard = () => {
  const [thismonthList, setThisMonthList] = useState();
  const [todayList, setTodayList] = useState();
  const [previousList, setPreviousList] = useState();
  const [futureList, setFutureList] = useState();
  const access = sessionStorage.getItem("userAccess");
  const user = JSON.parse(sessionStorage.getItem("user-details"));

  useEffect(() => {
    const getToday = async () => {
      await fetchEvent(`today`, setTodayList);
    };
    getToday();

    const getPrevious = () => {
      fetchEvent(`previous`, setPreviousList);
    };
    getPrevious();

    const getFuture = () => {
      fetchEvent(`future`, setFutureList);
    };
    getFuture();

    const getAllEvent = () => {
      fetchEvent(`thismonth`, setThisMonthList);
    };

    getAllEvent();
  }, [access]);

  let fetchEvent = (listSequence, setData) => {
    axios
      .get(`${BaseUrl}/events/${listSequence}`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setData(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const { useremail } = useParams();

  return (
    <div className={"Dashboard"}>
      <div className={"Dashboard-Header"}>
        <div className={"user-details"}>
          <h2>Welcome {user.name}!</h2>
        </div>
        <div className={"Header-text"}>
          <h2>Statistics</h2>
        </div>
        <div
          className={"Dashboard-link"}
          onClick={() => {
            fetchEvent();
          }}
        >
          <Link
            to={`/user/${useremail}/allevent`}
            className={"Dashboard-Card allevent"}
          >
            <FaListAlt className={"dashboard-icon"} />
            <h4>All Event</h4>
            <div className={"counter"}>
              {thismonthList && <p>{thismonthList.length}</p>}
            </div>
          </Link>
          <Link
            to={`/user/${useremail}/present`}
            className={"Dashboard-Card progress"}
          >
            <FaClock className={"dashboard-icon"} />
            <h4>Today Event</h4>
            <div className={"counter"}>
              {todayList && <p>{todayList.length}</p>}
            </div>
          </Link>
          <Link
            to={`/user/${useremail}/passed`}
            className={"Dashboard-Card complete"}
          >
            <FaCheckCircle className={"dashboard-icon"} />
            <h4>Previous Event</h4>
            <div className={"counter"}>
              {previousList && <p>{previousList.length}</p>}
            </div>
          </Link>
          <Link
            to={`/user/${useremail}/future`}
            className={"Dashboard-Card incomplete"}
          >
            <FaTimesCircle className={"dashboard-icon"} />
            <h4>Future Event</h4>
            <div className={"counter"}>
              {futureList && <p>{futureList.length}</p>}
            </div>
          </Link>
        </div>
      </div>
      <div className={"dashboard-body"}>
        <div className={"Dashboard-list"}>
          <h2>Today Event</h2>
          <div>
            <PList len={2} presentQuery={""} />
            <Link className={"seeMore"} to={`/user/${useremail}/present`}>
              View More...
            </Link>
          </div>
        </div>
        <div className={"Dashboard-list"}>
          <h2>Future Event</h2>
          <div>
            <FutureE len={2} futureQuery={""} />
            <Link className={"seeMore"} to={`/user/${useremail}/future`}>
              View More...
            </Link>
          </div>
        </div>
        <div className={"Dashboard-list"}>
          <h2>Previous Event</h2>
          <div>
            <Complete len={2} passQuery={""} />
            <Link className={"seeMore"} to={`/user/${useremail}/passed`}>
              View More...
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;

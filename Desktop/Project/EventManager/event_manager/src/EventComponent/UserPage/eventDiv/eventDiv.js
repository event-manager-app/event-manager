import { useState } from "react";
import { FiCalendar, FiClock, FiMoreHorizontal } from "react-icons/fi";
import { useParams, useNavigate } from "react-router";
import EditEvent from "../Event/EditEvent/EditEvent";
import "./eventDiv.css";

const EventDiv = ({
  title,
  id,
  description,
  starttime,
  date,
  onEdit,
  DeleteEvent,
  checkEdit,
  fetchEvt,
  showForm,
  publics,
  home,
}) => {
  const navigate = useNavigate();
  const [showEdit, setShowEdit] = useState(false);
  var today = new Date();

  const handleChange = (e) => {
    // const { checked } = e.target;
    checkEdit(id);
  };

  const viewBoard = (userId) => {
    navigate(`/${userId}`);
  };

  var dates = `${today.getFullYear()}-0${
    today.getMonth() + 1
  }-${today.getDate()}`;

  return (
    <div
      style={{
        backgroundColor: `${
          dates > date ? "#040d19" : dates === date && "#ecedf0"
        }`,
      }}
      className={"Event_Container"}
      onDoubleClick={() => {
        viewBoard(id);
      }}
    >
      <div className={"Event_Header"}>
        <h3 className={"ellips"}>{title}</h3>
        {!home && (
          <FiMoreHorizontal
            className={"Edit_icon"}
            onClick={() => {
              // toggleEdit(id);
              setShowEdit(!showEdit);
            }}
          />
        )}
        <EditEvent
          passId={id}
          eEdit={showEdit}
          onClosed={() => DeleteEvent(id)}
          fetchEvt={fetchEvt}
          onEdits={onEdit}
          showForm={showForm}
          publics={publics}
          date={date}
        />
      </div>
      <div className={"event_body"}>
        <p className={"ellips"}>{description}</p>
      </div>

      <div className={"event_time"}>
        <div>
          <FiClock className={"date-icons"} />
          <p>{starttime}</p>
        </div>

        <div>
          <FiCalendar className={"date-icons"} />
          <p>{date}</p>
        </div>
      </div>
    </div>
  );
};

export default EventDiv;

import React from "react";
import { FaUserAlt, FaRegSun, FaThList } from "react-icons/fa";
import { useNavigate } from "react-router";
import "./ProfileDropDown.css";

const ProfileDropDown = ({ showProfile }) => {
  const navigate = useNavigate();
  if (!showProfile) {
    return null;
  }

  return (
    <div className={"profileEdit"}>
      <div className={"profileEditDiv"}>
        <div>
          <button>Profile</button>
          <FaUserAlt />
        </div>
        <div>
          <button>Message</button>
          <FaThList />
        </div>
        <div>
          <button>Account Setting</button>
          <FaRegSun />
        </div>
        <div>
          <button
            onClick={() => {
              sessionStorage.removeItem("userAccess");
              sessionStorage.removeItem("user-details");
              navigate("/signin");
            }}
          >
            Sign Out
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProfileDropDown;

import axios from "axios";
import { useState, useEffect } from "react";
import BaseUrl from "../../../config";
import Complete from "../Event/CompleteEvent/Complete";
import "../Event/EList/EventList.css";

const Passed = () => {
  const [searchDetails, setSearchDetails] = useState([]);
  const [searchEvent, setSearchEvent] = useState([]);
  const [query, setQuery] = useState("");

  const access = sessionStorage.getItem("userAccess");

  useEffect(() => {
    fetchEvent();
  }, []);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/previous`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setSearchEvent(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    let resentSearch = searchEvent.filter((item) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );

    setSearchDetails(resentSearch);
  };

  return (
    <div className={"complete"}>
      <div className="allListInput">
        <input
          className={"ElistInput"}
          type="text"
          placeholder={"Search for Event"}
          value={query}
          onChange={handleChange}
        />
      </div>
      <div className={"allListHeader"}>
        <h2>PREVIOUS EVENT</h2>
      </div>
      <Complete passCount={searchDetails} passQuery={query} />
    </div>
  );
};

export default Passed;

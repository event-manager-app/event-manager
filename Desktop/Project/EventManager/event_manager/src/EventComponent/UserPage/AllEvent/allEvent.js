import axios from "axios";
import { useState, useEffect } from "react";
import BaseUrl from "../../../config";
import EventList from "../Event/EList/EventList";
import "../Event/EList/EventList.css";

const AllEvent = (e) => {
  const [searchDetails, setSearchDetails] = useState([]);
  const [searchEvent, setSearchEvent] = useState([]);
  const [query, setQuery] = useState("");

  const access = sessionStorage.getItem("userAccess");

  useEffect(() => {
    fetchEvent();
  }, []);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/allevents`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setSearchEvent(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    let resentSearch = searchEvent.filter((item) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );

    setSearchDetails(resentSearch);
  };

  return (
    <div className={"allList"}>
      <div className="allListInput">
        <input
          className={"ElistInput"}
          type="text"
          value={query}
          placeholder={"Search for Event"}
          onChange={handleChange}
        />
      </div>
      <div className={"allListHeader"}>
        <h2>ALL EVENT</h2>
      </div>
      <EventList
        listCount={searchDetails}
        listQuery={query}
        listSequence={"allevents"}
      />
    </div>
  );
};

export default AllEvent;

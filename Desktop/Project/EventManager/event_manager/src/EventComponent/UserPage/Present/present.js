import React, { useState, useEffect } from "react";
import PList from "../Event/ProgressList/PList";
import "../Event/EList/EventList.css";
import axios from "axios";
import BaseUrl from "../../../config";

const Present = () => {
  const [searchDetails, setSearchDetails] = useState([]);
  const [searchEvent, setSearchEvent] = useState([]);
  const [query, setQuery] = useState("");

  const access = sessionStorage.getItem("userAccess");

  useEffect(() => {
    fetchEvent();
  }, []);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/today`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setSearchEvent(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    let resentSearch = searchEvent.filter((item) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );

    setSearchDetails(resentSearch);
  };

  return (
    <div className={"present"}>
      <div className="allListInput">
        <input
          className={"ElistInput"}
          type="text"
          value={query}
          placeholder={"Search for Event"}
          onChange={handleChange}
        />
      </div>
      <div className={"allListHeader"}>
        <h2>TODAY'S EVENT</h2>
      </div>
      <PList presentCount={searchDetails} presentQuery={query} />
    </div>
  );
};

export default Present;

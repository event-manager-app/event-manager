import axios from "axios";
import { useState, useEffect } from "react";
import {
  FaOutdent,
  FaExpand,
  FaSearch,
  FaEnvelope,
  FaBell,
  FaTimes,
  FaChevronDown,
  FaBars,
} from "react-icons/fa";
import { useNavigate, useParams } from "react-router";
import BaseUrl from "../../../config";
import avatar from "../../../images/avatar1.png";
import ProfileDropDown from "../profileDropDown/ProfileDropDown";
import "./topbar.css";

const Topbar = ({ reduce, ShowSideBar }) => {
  const navigate = useNavigate();
  const [searchDetails, setSearchDetails] = useState();
  const [showInput, setShowInput] = useState(false);
  const [showTopbar, setShowTopbar] = useState(false);
  const [show, setShow] = useState(false);
  const [query, setQuery] = useState("");
  const [searchEvent, setSearchEvent] = useState([]);
  const [showProfile, setShowProfile] = useState(false);

  const access = sessionStorage.getItem("userAccess");

  useEffect(() => {
    fetchEvent();
  }, []);

  let fetchEvent = () => {
    axios
      .get(`${BaseUrl}/events/allevents`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          authorization: `Bearer ${access}`,
        },
      })
      .then((res) => {
        setSearchEvent(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChage = (e) => {
    const { value } = e.target;
    setQuery(value);
    // setShow(true);
    let resentSearch = searchEvent.filter((item) =>
      item.name.toLowerCase().includes(value.toLowerCase())
    );
    if (value !== "") {
      setSearchDetails(resentSearch);
    } else {
      setSearchDetails();
    }
    if (showInput === false) {
      return (value = "");
    }
  };

  const ShowInput = () => {
    if (showInput === false) {
      setShowInput(true);
      setShowTopbar(false);
    }
  };

  const onClose = () => {
    if (showTopbar === false) {
      setShowTopbar(true);
      setShowInput(false);
      setQuery("");
    }
  };

  return (
    <div className={"topbarMain"}>
      {showInput === true && showTopbar === false ? (
        <div className={"search"}>
          <div className={"searchDiv"}>
            <input
              className={"topBarInput"}
              type="text"
              value={query}
              name="searchbox"
              placeholder="Search..."
              onChange={handleChage}
            />

            <FaTimes className={"close"} onClick={onClose} />
          </div>

          {query !== undefined || show === true ? (
            <div>
              {console.log(searchDetails)}
              {searchDetails !== undefined && (
                <div className={"searchbody"}>
                  {query !== "" &&
                    searchDetails.map((item, key) => (
                      <div
                        className={"searchResult"}
                        key={key}
                        onClick={() => {
                          navigate(`/${item._id}`);
                          setShowInput(false);
                          setShowTopbar(true);
                          setSearchDetails();
                          setQuery("");
                        }}
                      >
                        <h3>{item.name}</h3>
                      </div>
                    ))}
                </div>
              )}
            </div>
          ) : (
            ""
          )}
        </div>
      ) : (
        <div className={"topBar"}>
          <div className={"sub-topBar"}>
            <div className={"topBar-div handbugger"} onClick={ShowSideBar}>
              <FaBars />
            </div>

            <div className={"topBar-div showFull"} onClick={reduce}>
              <FaOutdent />
            </div>
            {/* <div className={"topBar-div showFull"} onClick={remove}>
              <FaExpand />
            </div> */}
            <div className={"topBar-div"} onClick={ShowInput}>
              <FaSearch />
            </div>
          </div>
          <div className={"sub-topBar"}>
            <div className={"topBar-div"}>
              <FaEnvelope />
            </div>
            <div className={"topBar-div"}>
              <FaBell />
            </div>
            <div className={"topBar-div"}>
              <div
                className={"imageView"}
                onClick={() => {
                  setShowProfile(!showProfile);
                }}
              >
                <img
                  className={"img-div"}
                  src={avatar}
                  alt={"Avatar image"}
                  width={36}
                  height={36}
                />
                <FaChevronDown />
              </div>
              <ProfileDropDown
                className={"imagedropdown"}
                showProfile={showProfile}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Topbar;

import React from "react";
import vector from "../../images/3573382.jpg";
import EventDiv from "../UserPage/eventDiv/eventDiv";
import "./home.css";

const Home = () => {
  const storeData = JSON.parse(localStorage.getItem("event"));
  var today = new Date();
  var dates = `${today.getFullYear()}-0${
    today.getMonth() + 1
  }-${today.getDate()}`;

  return (
    <div className={"homeBody"}>
      <div className={"event-tracker-subHeader"}>
        <div className={"event-subHeader-text"}>
          <h1>GODFIRST EVENT MANAGER</h1>
          <h4>
            We Help You Keep Track Of All Your Even And Also To Help You Know
            Where You Where Lacking Behind Per Day
          </h4>
        </div>
        <div>
          <img src={vector} alt="" class={"homepage-image"} />
        </div>
      </div>
      <div className="display-main">
        <div>
          <h2>All Public Events</h2>
        </div>
        <div className="div-display">
          {storeData.map((item, key) => (
            <div>
              <EventDiv
                title={item.name}
                id={item._id}
                starttime={item.etime}
                date={item.stime}
                description={item.description}
                publics={item.public}
                home="home"
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;

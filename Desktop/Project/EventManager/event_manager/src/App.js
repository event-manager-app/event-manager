import { Routes, Route, Switch, useLocation, Navigate } from "react-router-dom";
import EventBoard from "./EventComponent/UserPage/Event/EventBoard/EventBoard";
import SideBar from "./EventComponent/UserPage/sidebar/sidebar";
import Topbar from "./EventComponent/UserPage/topbar/topbar";
import Dashboard from "./EventComponent/UserPage/DashBoard/Dashboard";
import Passed from "./EventComponent/UserPage/Passed/passed";
import Present from "./EventComponent/UserPage/Present/present";
import Future from "./EventComponent/UserPage/Future/future";
import AllEvent from "./EventComponent/UserPage/AllEvent/allEvent";
import "./App.css";
import React, { useState, useEffect } from "react";
import Home from "./EventComponent/Home/Home";
import Contact from "./EventComponent/Contact/contact";
import Service from "./EventComponent/Service/service";
import About from "./EventComponent/About/About";
import SignIn from "./EventComponent/loginDetails/signIn";
import SignUp from "./EventComponent/loginDetails/signUp";
import TopBars from "./EventComponent/topBars/topBars";
import UserView from "./EventComponent/UserPage/userView/userView";
import Footer from "./EventComponent/footer/footer";
import Error from "./EventComponent/error";
import ProtectedRoute from "./EventComponent/protectedRoute";

function App() {
  let location = useLocation();
  const [reduceSideBar, setReduceSideBar] = useState(false);
  const [showSideBar, setShowSideBar] = useState(false);

  const access = sessionStorage.getItem("userAccess");

  let ReduceSideBar = () => {
    setReduceSideBar(!reduceSideBar);
  };

  let ShowSideBar = () => {
    setShowSideBar(!showSideBar);
  };

  let unShowSideBar = () => {
    setShowSideBar(false);
  };

  return (
    <div className="App">
      {access && (
        <SideBar
          reduce={reduceSideBar}
          onclick={unShowSideBar}
          showSideBar={showSideBar}
        />
      )}

      <div className={"sub-app"}>
        {access && <Topbar reduce={ReduceSideBar} ShowSideBar={ShowSideBar} />}
        {!access && <TopBars />}

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/service" element={<Service />} />
          <Route path="/about" element={<About />} />
          <Route exact path="/contact" element={<Contact />} />
          <Route path="/signin" element={<SignIn />} />
          <Route path="/signup" element={<SignUp />} />)
          <Route element={<ProtectedRoute accessKey={access} />}>
            <Route path="/user" element={<Dashboard />} />
            <Route path="/create" element={<EventBoard />} />
            <Route path="/allevent" element={<AllEvent />} />
            <Route path="/present" element={<Present />} />
            <Route path="/future" element={<Future />} />
            <Route path="/passed" element={<Passed />} />
            <Route path="/:userId" element={<UserView />} />
          </Route>
        </Routes>
        {!access && <Footer />}
      </div>
    </div>
    // </Routers>
  );
}

export default App;
